package com.jeek.hw3;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Emil
 */
public class Register extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
             HttpSession session = request.getSession(true);
             
            RequestDispatcher rd=request.getRequestDispatcher("Registr.html");
            rd.include(request, response);
           
            String realUsername;
            realUsername = (String)request.getParameter("rusername");
            session.setAttribute("rUsername", realUsername);
            
            String realPassword;
            realPassword = (String)request.getParameter("rpassword");
            session.setAttribute("rPassword", realPassword);
            System.out.println("Hallo");
            String realColor;
            realColor = request.getParameter("rcolor");
            session.setAttribute("rColor",realColor);
 String reaColor;
            reaColor = request.getParameter("rcolor");
            session.setAttribute("Color",reaColor); 
            realColor = request.getParameter("rcolor");
            session.setAttribute("rColor",realColor);
            
            String firstName;
            firstName =(String)request.getParameter("name");
            session.setAttribute("fName", firstName);
            String lastName;
            lastName=(String) request.getParameter("lastname");
            session.setAttribute("lName", lastName);
            
            if (realUsername != null && realPassword != null && realUsername!="" && realPassword!="")
             {
                 session.setAttribute("Registered", "yes");
                    response.sendRedirect("Login");

             }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
