package com.jeek.hw3;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Emil
 */
public class Login extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
           
            String errorMessage;
            HttpSession session = request.getSession(true);
             
            RequestDispatcher dispatcher=request.getRequestDispatcher("Login.html");
            dispatcher.include(request, response);
            
            String loginUsername = (String)request.getParameter("login");
            String loginPassword = (String)request.getParameter("lpassword");
            
            
            String paramUsername = (String)session.getAttribute("rUsername");
            String paramPassword = (String)session.getAttribute("rPassword");          
             
             if (loginUsername == null && loginPassword ==null && loginUsername!="" && loginPassword!=""){
                 out.println(errorMessage ="<div align=\"center\">Please Log In</div>");
             }
             else {
                 if (loginUsername.equals(paramUsername)){
                     if(loginPassword.equals(paramPassword))
                         response.sendRedirect("CookieServlet");
                     else  out.println(errorMessage = "Your password is wrong");

                 }
                 else  out.println(errorMessage = "Your password is wrong");
             }
            
    }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    }
